﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ProjectDomi.Models;

namespace ProjectDomi.Controllers
{
    [Route("Home/[controller]")]
    public class HeroesController :Controller
    {
        [ActionName("GetPerson")]
        [HttpGet("{firstName}/{lastName}")]
        public Person Get(string firstName, string lastName)
        {
            var p = new Person() { FirstName = firstName, LastName = lastName };

            p.SetHeroName();

            return p;
        }


    }
}
