﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ProjectDomi.Models
{
    public class HeroGenerator
    {

        public static string GetHeroName(string firstName, string lastName)
        {
            if(String.IsNullOrEmpty(firstName) || String.IsNullOrEmpty(lastName))
            {
                return null;
            }
            var f = firstName.Substring(0, 1) ;
            var l = lastName.Substring(0, 1) ?? "";

            return string.Concat(GetHeroFirst(f)," ", GetHeroLast(l));
        }

        public static string GetHeroFirst(string f)
        {
            return new HeroFirst()[f.ToUpper()];
        }

        public static string GetHeroLast(string l)
        {
            return new HeroLast()[l.ToUpper()];
        }

        public class HeroFirst : Dictionary<string, string>
        {
            private void AddValue(string letter, string value)
            {
                Add(letter, value);
            }

            public HeroFirst()
            {
                foreach(var i in _firstNames)
                {
                    AddValue(i.Key, i.Value);
                }

            }

            private Dictionary<string, string> _firstNames = new Dictionary<string, string>()
        {
                {"","" },
            {"A", "Captain"},
            {"B", "Night" },
            {"C", "The" },
            {"D", "Ancient" },
            {"E", "Spider" },
            {"F", "Invisible" },
            {"G", "Master" },
            {"H", "Invincible" },
            {"I", "Silver" },
            {"J", "Flawless" },
            {"K", "Professor" },
            {"L", "Radioactive" },
            {"M", "Incredible" },
            {"N", "Impossible" },
            {"O", "Iron" },
            {"P", "Rocket" },
            {"Q", "Human" },
            {"R", "Power" },
            {"S", "Green" },
            {"T", "Super" },
            {"U", "Wonder" },
            {"V", "Metal" },
            {"W", "Doctor" },
            {"X", "Masked" },
            {"Y", "Crimson" },
            {"Z", "Omega" }
        };


        }

        public class HeroLast : Dictionary<string, string>
        {
            private void AddValue(string letter, string value)
            {
                Add(letter, value);
            }

            public HeroLast()
            {
                foreach(var i in _lastNames)
                {
                    AddValue(i.Key, i.Value);
                }
            }


            private Dictionary<string, string> _lastNames = new Dictionary<string, string>()
        {
                {"","" },
            {"A", "Lightning"},
            {"B", "Knight" },
            {"C", "Hulk" },
            {"D", "Centurion" },
            {"E", "Surfer" },
            {"F", "Warrior" },
            {"G", "Soldier" },
            {"H", "Ghost" },
            {"I", "Master" },
            {"J", "Hornet" },
            {"K", "Phantom" },
            {"L", "Crusader" },
            {"M", "Daredevil" },
            {"N", "Machine" },
            {"O", "America" },
            {"P", "X" },
            {"Q", "Cyclone" },
            {"R", "Fist" },
            {"S", "Shadow" },
            {"T", "Patriot" },
            {"U", "Claw" },
            {"V", "Ninja" },
            {"W", "Storm" },
            {"X", "Samurai" },
            {"Y", "Yeller" },
            {"Z", "Man" }
        };


        }


    }
}
